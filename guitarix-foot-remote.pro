#-------------------------------------------------
#
# Project created by QtCreator 2020-06-07T11:46:57
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = guitarix-foot-remote
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

RASPI:DEFINES += RASPI
RASPI:LIBS += -lwiringPi

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++14

SOURCES += \
        guitarixrpc.cpp \
        jsonrpc.cpp \
        main.cpp \
        mainwindow.cpp \
        state.cpp

RASPI:SOURCES += \
        hardware.cpp

HEADERS += \
        guitarixrpc.h \
        nlohmann/json.hpp \
        jsonrpc.h \
        mainwindow.h \
        state.h \
        types.h

RASPI:HEADERS += \
        hardware.h

FORMS += \
        mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resources.qrc
