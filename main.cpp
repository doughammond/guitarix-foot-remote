#include <QApplication>

#include "guitarixrpc.h"
#include "state.h"
#include "mainwindow.h"


#ifdef RASPI
#include "hardware.h"
#endif

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    State s;

    GuitarixRpc rpc(7000);
    // RPC to state
    rpc.connect(&rpc, &GuitarixRpc::receivedBanks, &s, &State::receiveBankList);
    rpc.connect(&rpc, &GuitarixRpc::receivedPresets, &s, &State::receivePresetList);
    rpc.connect(&rpc, &GuitarixRpc::receivedUnitList, &s, &State::receiveUnitList);
    rpc.connect(&rpc, &GuitarixRpc::receivedUnitState, &s, &State::receiveUnitState);
    rpc.connect(&rpc, &GuitarixRpc::presetChanged, &s, &State::receiveCurrentBankPreset);
    // state to RPC
    s.connect(&s, &State::bankSelect, &rpc, &GuitarixRpc::onBankSelect);
    s.connect(&s, &State::presetSelect, &rpc, &GuitarixRpc::onPresetSelect);
    s.connect(&s, &State::unitSwitch, &rpc, &GuitarixRpc::onUnitSwitch);

    MainWindow w;
    // State to UI
    w.connect(&s, &State::pageChange, &w, &MainWindow::onPageChange);
    // UI to State
    w.connect(&w, &MainWindow::previous, &s, &State::onPrev);
    w.connect(&w, &MainWindow::next, &s, &State::onNext);
    w.connect(&w, &MainWindow::page, &s, &State::onPagePress);
    w.connect(&w, &MainWindow::button, &s, &State::onButtonPress);

#ifdef RASPI
    Hardware hw;
    // Buttons to State
    hw.connect(&hw, &Hardware::previous, &s, &State::onPrev);
    hw.connect(&hw, &Hardware::next, &s, &State::onNext);
    hw.connect(&hw, &Hardware::page, &s, &State::onPagePress);
    hw.connect(&hw, &Hardware::button, &s, &State::onButtonPress);

    // Additional button functions
    hw.connect(&hw, &Hardware::page, [](const bool longPress) {
        if (longPress) {
            qDebug() << "PAGE LONG PRESS - Shutdown";
            system("shutdown -P now");
        }
    });
#endif

    w.show();

    rpc.ready();
    s.ready();

    return a.exec();
}
