#pragma once

#include <QObject>
#include <QThread>
#include <QMap>
#include <QDebug>
#include <QTimer>

#include <wiringPi.h>

// ----

class InterruptWorker : public QObject
{
    Q_OBJECT

public slots:
    void watchPin(const quint8 pin) {
        int state = 0;
//        int iter = 0;
        while (m_running) {
            int i = digitalRead(pin);
            if (i == HIGH && state == 0) {
                state = 1;
                qInfo() << "watch pin" << pin << "got change ACTIVE";
                emit interrupt(pin, true);
                // qInfo() << "emit interrupt finished";
            } else if (i == LOW && state == 1) {
                state = 0;
                qInfo() << "watch pin" << pin << "got change INACTIVE";
                emit interrupt(pin, false);
                // qInfo() << "emit interrupt finished";
            }
            delay(1);
//            if (++iter == 1000) {
//                // qInfo() << "interrupt poll alive : " << i << " / " << state;
//                iter = 0;
//                if (i == 1 && state == 1) {
//                    // qInfo() << "reset int detect state";
//                    state = 0;
//                }
//            }
        }
        qDebug() << "worker loop exit";
    }

    void stop() {
        qDebug() << "worker set end";
        m_running = false;
    }

signals:
    void interrupt(const quint8 pin, const bool active);

private:
    bool m_running = true;
};

class InterruptHandler : public QObject
{
    Q_OBJECT

public:
    InterruptHandler(QObject *parent = nullptr);
    ~InterruptHandler();

    void setup(const quint8 pin);
    void stop();

signals:
    void begin();
    void interrupt(const quint8 pin, const bool active);

private:
    QMap<quint8, InterruptWorker*> m_workers;
    QMap<quint8, QThread*> m_threads;
};

// ----

class Hardware : public QObject
{
    Q_OBJECT
public:
    explicit Hardware(QObject *parent = nullptr);
    ~Hardware();

public slots:
    void onInterrupt(const quint8 pin, const bool active);
    void sendPress(const quint8 pin, const bool active);

signals:
    void previous(const bool longPress);
    void next(const bool longPress);
    void page(const bool longPress);
    void button(const quint8 idx, const bool longPress);

private:
    InterruptHandler m_inthandler;
    quint8 m_lastPin;
    QTimer m_longPressTimer;
};
