#include <iostream>
#include <sstream>

#include <QDebug>
#include <QTimer>

#include "jsonrpc.h"

Jsonrpc::Jsonrpc(const quint16 port, QObject *parent)
    : QObject(parent)
    , m_port(port)
    , m_callId(0)
{
    connect(&m_socket, &QTcpSocket::stateChanged, this, &Jsonrpc::onSocketState);
    connect(&m_socket, &QTcpSocket::connected, this, &Jsonrpc::onConnected);
    connect(&m_socket, &QTcpSocket::readyRead, this, &Jsonrpc::onReadyRead);
    // TODO: error handling
    // TODO: reconnection
}

void Jsonrpc::ready()
{
    m_socket.connectToHost("127.0.0.1", m_port);
}

void Jsonrpc::onSocketState()
{
    qDebug() << "onSocketState" << m_socket.state();
    if (m_socket.state() == QAbstractSocket::UnconnectedState) {
        qDebug() << "reconnecting ...";
        QTimer::singleShot(1000, this, &Jsonrpc::ready);
    }
}

void Jsonrpc::onConnected()
{
    emit connected();
}

void Jsonrpc::onReadyRead()
{
    while (m_socket.canReadLine()) {
        const QByteArray response = m_socket.readLine();
        nlohmann::json doc = NULL;
        try {
            // qDebug() << "JSON response" << response;
            doc = nlohmann::json::parse(response.toStdString());
        } catch (std::exception &e) {
            std::cerr << "error receiving json `" << response.toStdString() << "` : " << e.what() << std::endl;
            continue;
        }
        if (doc.is_object()) {
            try {
                emit handleMessage(doc);
            } catch (std::exception &e) {
                std::cerr << "error handling message `" << response.toStdString() << "` : " << e.what() << std::endl;
            }
            try {
                // rpc callbacks
                if (doc.contains("id")) {
                    const auto callIdStr = doc["id"].get<std::string>();
                    const auto callId = std::stoi(callIdStr);
                    qDebug() << "got reply for callId " << callId;
                    const auto cbi = m_deferreds.find(callId);
                    if (cbi != m_deferreds.end()) {
                        qDebug() << "have valid callback for id" << callId;
                        m_deferreds[callId].resolve(doc["result"]);
                        qDebug() << "deferred resolved for id" << callId;
                        m_deferreds.remove(callId);
                        m_promises.remove(callId);
                    }
                    return;
                }
            } catch (std::exception &e) {
                std::cerr << "error issuing callback `" << response.toStdString() << "` : " << e.what() << std::endl;
            }
        }
    }
}

nlohmann::json toJson(const QVariantList &vl) {
    nlohmann::json a = nlohmann::json::array();
    for (const auto &v : vl) {
        switch (v.type()) {
        case QVariant::String:
            a.push_back(v.toString().toStdString());
            break;
        case QVariant::Int:
            a.push_back(v.toInt());
            break;
        default:
            qDebug() << "toJson() : unhandled variant type " << v.type();
            break;
        }
    }
    return a;
}

QtPromise::Promise<json> Jsonrpc::call(const QString &method, const QVariantList &params)
{
    nlohmann::json req;
    req["id"] = m_callId;
    req["method"] = method.toStdString();
    req["params"] = toJson(params);

    auto def = QtPromise::Deferred<json>();
    m_deferreds[m_callId] = def;
    auto prm = def.promise();
    m_promises[m_callId] = &prm;
    m_callId++;

    rpcRequest(req);

    return prm;
}

void Jsonrpc::notify(const QString &method, const QVariantList &params)
{
    nlohmann::json req;
    req["method"] = method.toStdString();
    req["params"] = toJson(params);
    rpcRequest(req);
}

void Jsonrpc::rpcRequest(const nlohmann::json &doc)
{
    std::stringstream ss;
    ss << doc;
    const auto req = QByteArray(ss.str().c_str());
    qDebug() << "sending" << req;
    m_socket.write(req + "\n");
}
