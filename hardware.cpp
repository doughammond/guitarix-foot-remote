#include "hardware.h"

// ----

InterruptHandler::InterruptHandler(QObject *parent) : QObject(parent)
{

}

InterruptHandler::~InterruptHandler()
{
    stop();
}

void InterruptHandler::setup(const quint8 pin)
{
    if (m_workers.contains(pin)) {
        qInfo() << "Already working on pin" << pin;
        return;
    }
    auto *worker = new InterruptWorker();
    m_workers[pin] = worker;
    QThread *t = new QThread();
    worker->moveToThread(t);
    m_threads[pin] = t;
    connect(
        t, &QThread::finished,
        worker, &QObject::deleteLater
    );
    connect(
        t, &QThread::started,
        [=](){
            worker->watchPin(pin);
        }
    );
    connect(
        worker, &InterruptWorker::interrupt,
        this, &InterruptHandler::interrupt
    );
    t->start();
}

void InterruptHandler::stop()
{
    qDebug() << "stop workers";
    for (auto &&w : m_workers) {
        // qDebug() << ".s.";
        w->stop();
    }
    m_workers.clear();

    qDebug() << "stop threads";
    for (auto &&t : m_threads) {
        // qDebug() << ".q.";
        t->quit();
        // qDebug() << ".w.";
        t->wait();
    }
    // qDebug() << "clear threads";
    m_threads.clear();
    // qDebug() << "done";
}

// ----

#define PIN_PREV 0  // BCM 17 / PHY 11
#define PIN_NEXT 2  // BCM 27 / PHY 13
#define PIN_PAGE 3  // BCM 22 / PHY 15
#define PIN_BTN0 23 // BCM 13 / PHY 33 
#define PIN_BTN1 4  // BCM 23 / PHY 16
#define PIN_BTN2 5  // BCM 24 / PHY 18

Hardware::Hardware(QObject *parent)
    : QObject(parent)
    , m_lastPin(255)
{
    wiringPiSetup();

    connect(&m_inthandler, &InterruptHandler::interrupt, this, &Hardware::onInterrupt);
    m_longPressTimer.setSingleShot(true);
    connect(&m_longPressTimer, &QTimer::timeout, [=](){
        if (m_lastPin != 255) {
            sendPress(m_lastPin, true);
        }
    });

    const auto setupPin = [=](const quint8 pin) {
        pinMode(pin, INPUT);
        pullUpDnControl(pin, PUD_DOWN);
        m_inthandler.setup(pin);
    };

    setupPin(PIN_PREV);
    setupPin(PIN_NEXT);
    setupPin(PIN_PAGE);
    setupPin(PIN_BTN0);
    setupPin(PIN_BTN1);
    setupPin(PIN_BTN2);
}

Hardware::~Hardware()
{
}

void Hardware::onInterrupt(const quint8 pin, const bool active)
{
    qDebug() << "onInterrupt" << pin << ":" << active;

    if (m_longPressTimer.isActive()) {
        m_longPressTimer.stop();
    }

    m_lastPin = active ? pin : 255;

    if (active) {
        sendPress(pin, false);
        m_longPressTimer.start(5000);
    }
}

void Hardware::sendPress(const quint8 pin, const bool longPress)
{
    qDebug() << "sendPress" << pin << ":" << longPress;
    switch (pin) {
    case PIN_PREV:
        emit previous(longPress);
        break;
    case PIN_NEXT:
        emit next(longPress);
        break;
    case PIN_PAGE:
        emit page(longPress);
        break;
    case PIN_BTN0:
        emit button(0, longPress);
        break;
    case PIN_BTN1:
        emit button(1, longPress);
        break;
    case PIN_BTN2:
        emit button(2, longPress);
        break;
    default:
        qDebug() << "unhandled pin interrupt" << pin;
    }
}
