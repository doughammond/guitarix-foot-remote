# Guitarix Foot Remote Control App

This little app intergrates with Guitarix's JSONRPC remote control protocol, to enable bank, preset and unit switching via hardware buttons on Raspberry Pi.

It is designed to be run under i3 tiling window manager, with Guitarix in the upper 85% of the screen space and this app in the lower 15% screen space, so that the app buttons line up with the hardware buttons on the unit.

The unit itself consists of:

- Raspberry Pi 4
- 5 inch HDMI touch screen
- 6x footswitch push buttons + debounce filter circuits: "Page", "Prev", "A", "B", "C", "Next".
- Audioinjector Ultra sound card

This repo also contains some hardware design documents; 3rd party component reference drawings and drawings for construction in a 240x125x50 enclosure.