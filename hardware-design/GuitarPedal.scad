// ---------------

$fn = 45;

// ---------------

// enclosure dimensions
box_width = 240;
box_height = 125;
box_depth = 50;

screw_hole_dia = 1.75;

module hole(dia) {
    difference() {
        circle(dia); // hole
        circle(0.11); // centre mark
    }
}

// screen parameters - waveshare 5" 800x480 touch (GPIO touch)
scr_disp_width = 110;
scr_disp_height = 66;
scr_unit_oh_left = 6;
scr_unit_oh_right = 4;
scr_unit_oh_top = 14;
scr_unit_oh_bottom = 9;
scr_unit_depth = 13;
scr_connector_left = 43;
scr_connector_bottom = 50;
scr_connector_width = 33;
scr_connector_height = 5;
scr_connector_depth = 15;
scr_hole_oh_left = 2.5;
scr_hole_oh_right = 1.5;
scr_hole_oh_top = 11;
scr_hole_oh_bottom = 6.5;

scr_usb_centre_y = 47.5;
scr_usb_top_z = 5;
scr_usb_con_height = 10;
scr_usb_con_width = 40;
scr_usb_con_depth =  7.5;

scr_hdmi_centre_x = 63;

// // stock rpi hdmi cable
// scr_hdmi_top_z = 4;
// scr_hdmi_con_height = 55;
// scr_hdmi_con_width = 21;
// scr_hdmi_con_depth = 13;

// waveshare hdmi micro adaptor
scr_hdmi_top_z = 8;scr_hdmi_con_height = 8;
scr_hdmi_con_width = 22;
scr_hdmi_con_depth = 18;

// // SlimHDMI Male Micro HDMI D - Male HDMI A 0.25m
// // approx dimensions
// scr_hdmi_top_z = 4;
// scr_hdmi_con_height = 25;
// scr_hdmi_con_width = 16;
// scr_hdmi_con_depth = 13;

scr_x = box_width / 2 - scr_disp_width / 2;
scr_y = box_height - scr_disp_height - scr_hole_oh_top - screw_hole_dia - 5;

module screen() {
    // screen display hole
    square([scr_disp_width, scr_disp_height]);

    // screen unit
    %translate([-scr_unit_oh_left, -scr_unit_oh_bottom, -scr_unit_depth - 1])
    cube([scr_unit_oh_left + scr_unit_oh_right + scr_disp_width, scr_unit_oh_top + scr_unit_oh_bottom + scr_disp_height, scr_unit_depth]);

    // screen gpio connector
    %translate([scr_connector_left - scr_unit_oh_left, scr_connector_bottom - scr_unit_oh_bottom, - 2*scr_unit_depth - 1])
    cube([scr_connector_width, scr_connector_height, scr_connector_depth]);

    // screen screw holes
    translate([-scr_hole_oh_left, scr_hole_oh_top + scr_disp_height]) hole(screw_hole_dia);
    translate([scr_disp_width + scr_hole_oh_right, scr_hole_oh_top + scr_disp_height]) hole(screw_hole_dia);
    translate([-scr_hole_oh_left, -scr_hole_oh_bottom]) hole(screw_hole_dia);
    translate([scr_disp_width + scr_hole_oh_right, -scr_hole_oh_bottom]) hole(screw_hole_dia);

    // usb connector space
    %translate([-scr_unit_oh_left - scr_usb_con_width, scr_usb_centre_y - (scr_usb_con_height/2), -scr_usb_top_z - scr_usb_con_depth])
    cube([scr_usb_con_width, scr_usb_con_height, scr_usb_con_depth]);

    // hdmi connector space
    %translate([scr_hdmi_centre_x - (scr_hdmi_con_width / 2), -scr_unit_oh_bottom - scr_hdmi_con_height, -scr_hdmi_top_z - scr_hdmi_con_depth])
    cube([scr_hdmi_con_width, scr_hdmi_con_height, scr_hdmi_con_depth]);
}

// footswitch hole paramters
fs_radius = 6;
fs_space = scr_disp_width / 2 - fs_radius;
fs_y = scr_y / 2.5;

// footswitch
module fs() {
    hole(fs_radius);
    %translate([-fs_radius, -fs_radius - 0.5, -17]) {
        cube([12, 13, 17]);
    }
}

// Top panel
module top_panel() {
    difference() {
        // panel
        square([box_width, box_height]);

        // screen hole
        translate([scr_x, scr_y]) screen();

        // footswitch holes bottom row
        translate([box_width / 2 - fs_space * 2, fs_y]) fs();
        translate([box_width / 2 - fs_space, fs_y]) fs();
        translate([box_width / 2, fs_y]) fs();
        translate([box_width / 2 + fs_space, fs_y]) fs();
        translate([box_width / 2 + fs_space * 2, fs_y]) fs();

        // footswitch holes screen side
        translate([box_width / 2 - fs_space * 2, scr_y + scr_disp_height / 2]) fs();
        translate([box_width / 2 + fs_space * 2, scr_y + scr_disp_height / 2]) fs();
    }
}

// Audio jack inputs/outputs
audio_socket_radius = 5.5;
audio_socket_x = [0, 21.5, 46.5, 68];
audio_socket_width = 79;

module audio_io() {
    translate([audio_socket_x[0], 0]) hole(audio_socket_radius);
    translate([audio_socket_x[1], 0]) hole(audio_socket_radius);
    translate([audio_socket_x[2], 0]) hole(audio_socket_radius);
    translate([audio_socket_x[3], 0]) hole(audio_socket_radius);
}

// Raspberry Pi 4
rpi_usb_width = 14;
rpi_usb_height = 15;
rpi_net_width = 15;
rpi_net_height = 13;
rpi_port_x = [1.5, 19.5, 37.5];
rpi_width = 56;
rpi_standoff_height = 6;

module rpi() {
    translate([rpi_port_x[0], 0]) square([rpi_usb_width, rpi_usb_height]);
    translate([rpi_port_x[1], 0]) square([rpi_usb_width, rpi_usb_height]);
    translate([rpi_port_x[2], 0]) square([rpi_net_width, rpi_net_height]);
}

rpi_x = box_width - rpi_width - 26;
rpi_y = rpi_standoff_height;
audio_io_x = 30;
audio_io_y = rpi_standoff_height + rpi_usb_height / 2;

power_width = 20;
power_x = box_width - (box_width - (rpi_x + rpi_width)) / 2;
power_y = rpi_usb_height + rpi_y - power_width / 2;

usb_c_hole_width = 9.0;
usb_c_hole_height = 3.0;
usb_c_socket_a_width = 28;
usb_c_socket_a_height = 5;
usb_c_socket_b_width = 13;
usb_c_socket_b_height = 45;

usb_c_socket_depth = 8;

// Power input
module power_usb() {
    translate([0, 0]) hole(screw_hole_dia);
    translate([power_width, 0]) hole(screw_hole_dia);
    
    translate([power_width / 2 - usb_c_hole_width / 2, -usb_c_hole_height / 2])
    square([usb_c_hole_width, usb_c_hole_height]);

    %translate([power_width / 2 - usb_c_socket_a_width / 2, -usb_c_socket_depth / 2])
    cube([usb_c_socket_a_width, usb_c_socket_depth, usb_c_socket_a_height]);
    
    %translate([power_width / 2 - usb_c_socket_b_width / 2, -usb_c_socket_depth / 2])
    cube([usb_c_socket_b_width, usb_c_socket_depth, usb_c_socket_b_height]);
}

// Rear panel
module rear_panel() {
    difference() {
        // panel
        square([box_width, box_depth]);
        translate([rpi_x, rpi_y]) rpi();

        translate([audio_io_x, audio_io_y]) audio_io();
        %translate([audio_io_x - audio_socket_radius, audio_io_y - 8]) {
            cube([audio_socket_width, 35, 30]);
        }

        translate([power_x, power_y]) rotate([0, 0, 90])
        power_usb();
    }
}

feet_hole_inset = 7;
feet_hole_x = [20, 220];
feet_hole_y = [box_height - feet_hole_inset, feet_hole_inset];

rpi_mount_x = [rpi_x + 3.5, rpi_x + rpi_width - 3.5];
rpi_mount_y = [27.5, 85];

ac_width = 38;
ac_depth = 80;
ac_screw_x = 3;
ac_screw_y = 4;
ac_screw_space_y = 57.7;
ac_screw_space_x = 23;
ac_x = 27;
ac_y = box_height / 2 - ac_depth / 2 + 9;

bc_depth = 81;
bc_width = 51;
bc_height = 26;
bc_spc = 73.5;
bc_x = 148;
bc_y = box_height - bc_width - 16;
bc_component_height = 5;

module bottom_panel() {
    difference() {
        // panel
        square([box_width, box_height]);

        // box screw / feet holes
        translate([feet_hole_x[0], feet_hole_y[0]]) hole(screw_hole_dia);
        translate([feet_hole_x[1], feet_hole_y[0]]) hole(screw_hole_dia);
        translate([feet_hole_x[0], feet_hole_y[1]]) hole(screw_hole_dia);
        translate([feet_hole_x[1], feet_hole_y[1]]) hole(screw_hole_dia);

        // rpi
        translate([rpi_mount_x[0], rpi_mount_y[0]]) hole(screw_hole_dia);
        translate([rpi_mount_x[1], rpi_mount_y[0]]) hole(screw_hole_dia);
        translate([rpi_mount_x[0], rpi_mount_y[1]]) hole(screw_hole_dia);
        translate([rpi_mount_x[1], rpi_mount_y[1]]) hole(screw_hole_dia);
        %translate([rpi_x, 0]) {
            // +3.5mm depth for the usb port overhang?
            cube([rpi_width, 85 + 3.5, 32]);
        }

        // audio card
        translate([ac_x + ac_screw_x, ac_y + ac_screw_y]) {
            translate([0, 0]) hole(screw_hole_dia);
            translate([0, ac_screw_space_y]) hole(screw_hole_dia);
            translate([ac_screw_space_x, 0]) hole(screw_hole_dia);
            translate([ac_screw_space_x, ac_screw_space_y]) hole(screw_hole_dia);
        }
        %translate([ac_x, ac_y]) {
            cube([ac_width, ac_depth, 28 + rpi_standoff_height]);
        }

        // button controller board
        translate([bc_x, bc_y]) rotate(90) {
            translate([bc_width / 2, 0]) {
                translate([0, 0]) hole(screw_hole_dia);
                translate([0, bc_spc]) hole(screw_hole_dia);
            }
            %translate([0, (bc_spc - bc_depth)/2]) {
                cube([bc_width, bc_depth, bc_component_height]);
                translate([bc_width / 2 - 6/2, bc_depth / 2 - 55 / 2]) cube([6, 55, bc_height]);
            }
        }
    }
}

// ---------------

// Assembly
union() {
    rotate(180) translate([-box_width, -box_height]) {
        bottom_panel();
    }

    translate([0, 0, box_depth]) {
        top_panel();
    }

    rotate([90]) scale([-1, 1]) translate([-box_width, 0, -box_height]) {
        rear_panel();
    }
}
