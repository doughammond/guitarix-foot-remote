#pragma once

#include <QString>
#include <QList>

struct Unit {
    QString name;
    QString label;
    quint8 enabled;
};
typedef QList<Unit> UnitList;
