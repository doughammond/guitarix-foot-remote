#include <iostream>

#include "guitarixrpc.h"

GuitarixRpc::GuitarixRpc(const quint16 port, QObject *parent)
    : QObject(parent)
    , m_rpc(port, this)
{
    connect(&m_rpc, &Jsonrpc::connected, this, &GuitarixRpc::onConnected);
    connect(&m_rpc, &Jsonrpc::handleMessage, this, &GuitarixRpc::onHandleMessage);
}

void GuitarixRpc::ready()
{
    m_rpc.ready();
}

void GuitarixRpc::onConnected()
{
    QVariantList params;
    params << "preset" << "presetlist_changed" << "param";
    m_rpc.notify("listen", params);
    getBanks();
}

void GuitarixRpc::onBankSelect(const QString &name)
{
    m_rpc.call("presets", { name })
    .then([=](nlohmann::json response){
        std::cout << "got presets for bank " << name.toStdString() << ": " << response << std::endl;
        QStringList presets;
        for (const auto &preset : response) {
            presets << QString::fromStdString(preset);
        }
        emit receivedPresets(name, presets);
    });
}

void GuitarixRpc::onPresetSelect(const QString &bank, const QString &preset)
{
    QVariantList params;
    params << bank << preset;
    m_rpc.notify("setpreset", params);
}

void GuitarixRpc::onUnitSwitch(const QString &name, const bool state)
{
    QVariantList params;
    params << (name + ".on_off") << static_cast<int>(state);
    m_rpc.notify("set", params);
    emit receivedUnitState(name, state);
}

void GuitarixRpc::onHandleMessage(const nlohmann::json &doc) {
    const bool isObj = doc.is_object();
//     qDebug() << "handleMessage: " << "is_object? " << isObj;

    if (isObj) {
        if (doc.contains("error")) {
            std::cerr << "received error response: " << doc << std::endl;
            return;
        }

        if (doc.contains("method")) {
            const auto method = doc["method"].get<std::string>();

            // preset list changes
            if (method == "presetlist_changed") {
                std::cout << "presetlist_changed " << doc << std::endl;
                getBanks();
                return;
            }

            // preset changes
            if (method == "preset_changed") {
                std::cout << "preset_changed " << doc << std::endl;
                const auto bank = doc["params"][0].get<std::string>();
                const auto preset = doc["params"][1].get<std::string>();
                emit presetChanged(
                    QString::fromStdString(bank),
                    QString::fromStdString(preset)
                );
                getUnits();
                return;
            }

            // param changes
            if (method == "set") {
                // parameter change
                const auto stdParamName = doc["params"][0].get<std::string>();
                const auto paramName = QString::fromStdString(stdParamName);
                if (paramName.endsWith(".on_off")) {
                    const auto state = doc["params"][1].get<quint8>();
                    qDebug() << "on_off change: " << paramName << " = " << state;
                    emit receivedUnitState(paramName.mid(0, paramName.size() - 7), state);
                }
                return;
            }
        }
    }
}

void GuitarixRpc::getBanks() {
    m_rpc.call("banks")
    .then([=](json response) {
//        std::cout << "banks are" << response << std::endl;
        QStringList banks;
        for (const auto &bankObj : response) {
            const auto bankName = bankObj["name"].get<std::string>();
//            std::cout << " : " << bankName << std::endl;
            const auto bank = QString::fromStdString(bankName);
            QStringList presets;
            for (const auto preset : bankObj["presets"]) {
                presets << QString::fromStdString(preset);
            }
            receivedPresets(bank, presets);
            banks << bank;
        }
        qDebug() << "bank names are" << banks;
        emit receivedBanks(banks);
    });
}

void GuitarixRpc::getUnits()
{
    for (quint8 i = 0; i < 2; ++i) {
        m_rpc.call("get_rack_unit_order", { i })
        .then([=](json units) {
            std::cout << "units are " << units << std::endl;
            QVariantList params;
            for (const auto &unit : units) {
                params << QString("%1.on_off").arg(QString::fromStdString(unit));
            }
            m_rpc.call("get_parameter", params)
            .then([=](nlohmann::json result) {
                std::cout << "get_parameter result= " << result << std::endl;
                UnitList unitList;
                for (auto & unit : units) {
                    const auto paramStr = unit.get<std::string>() + ".on_off";
                    const auto unitState = result[paramStr]["value"][paramStr].get<quint8>();
                    const auto unitLabel = result[paramStr]["group"].get<std::string>();
                    std::cout << " : " << unit << " = " << static_cast<int>(unitState) << std::endl;
                    unitList.push_back({
                        QString::fromStdString(unit),
                        QString::fromStdString(unitLabel),
                        unitState
                    });
                }
                emit receivedUnitList(i, unitList);
            });
        });
    }
}
