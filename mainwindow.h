#pragma once

#include <QMainWindow>

#include "state.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void onPageChange(const Page &page);

signals:
    void previous();
    void next();
    void page();
    void button(const quint8 idx);

private:
    Ui::MainWindow *ui;
};
