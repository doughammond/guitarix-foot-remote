#pragma once

#include <functional>

#include <QObject>
#include <QTcpSocket>

#include "nlohmann/json.hpp"
#include "qt-promise/promise.h"

#include "types.h"

using nlohmann::json;

class Jsonrpc : public QObject
{
    Q_OBJECT
public:
    explicit Jsonrpc(const quint16 port, QObject *parent = nullptr);

public slots:
    void ready();

    void onSocketState();
    void onConnected();
    void onReadyRead();

    QtPromise::Promise<json> call(const QString &method, const QVariantList &params = {});
    void notify(const QString &method, const QVariantList &params);

signals:
    void connected();
    void handleMessage(const nlohmann::json &doc);

private:
    void rpcRequest(const nlohmann::json &doc);

    quint16 m_port;
    QTcpSocket m_socket;

    int m_callId;
    QMap<int, QtPromise::Deferred<json>> m_deferreds;
    QMap<int, QtPromise::Promise<json>*> m_promises;
};
