#pragma once

#include <QObject>

#include "jsonrpc.h"

class GuitarixRpc : public QObject
{
    Q_OBJECT
public:
    explicit GuitarixRpc(const quint16 port, QObject *parent = nullptr);

    void ready();

public slots:
    void onConnected();
    void onBankSelect(const QString &name);
    void onPresetSelect(const QString &bank, const QString &preset);
    void onUnitSwitch(const QString &name, const bool state);
    void onHandleMessage(const nlohmann::json &doc);

signals:
    void presetChanged(const QString &bank, const QString &preset);
    void receivedBanks(const QStringList &banks);
    void receivedPresets(const QString &bank, const QStringList &presets);
    void receivedUnitList(const quint8 listNum, const UnitList &units);
    void receivedUnitState(const QString &name, const quint8 state);

private:
    void getBanks();
    void getUnits();

    Jsonrpc m_rpc;
};
