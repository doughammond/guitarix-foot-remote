#include <QDebug>

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(this->ui->btn_Prev, &QPushButton::pressed, this, &MainWindow::previous);
    connect(this->ui->btn_Next, &QPushButton::pressed, this, &MainWindow::next);
    connect(this->ui->btn_Page, &QPushButton::pressed, this, &MainWindow::page);
    connect(this->ui->lbl_A, &QPushButton::clicked, [=](){ emit button(0); });
    connect(this->ui->lbl_B, &QPushButton::clicked, [=](){ emit button(1); });
    connect(this->ui->lbl_C, &QPushButton::clicked, [=](){ emit button(2); });
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onPageChange(const Page &page)
{
    const QList<QToolButton*> btns = {
        this->ui->lbl_A,
        this->ui->lbl_B,
        this->ui->lbl_C
    };
    for (int i = 0; i < 3; ++i) {
        auto btn = btns[i];
        const auto tab = page.tabs[i];
        qDebug() << "onPageChange" << i << tab.name << ":" << tab.label << "=" << tab.active;

        QPalette pal;
        pal.setColor(QPalette::Button, QColor(255, 255, 255, 0));
        pal.setColor(QPalette::Highlight, QColor(255, 255, 255, 0));
        QIcon icon;
        auto font = btn->font();
        if (/* DISABLES CODE */ (0)) { // tab.type == Screen::UNIT) {
            const QString onIcon = QString(":/icons/graphics/unit-%1-on.svg").arg(tab.name);
            const QString offIcon = QString(":/icons/graphics/unit-%1-off.svg").arg(tab.name);
            const QSize iconSize(200, 200);
            icon.addFile(onIcon,  iconSize, QIcon::Normal, QIcon::On);
            icon.addFile(onIcon,  iconSize, QIcon::Active, QIcon::On);
            icon.addFile(onIcon,  iconSize, QIcon::Disabled, QIcon::On);
            icon.addFile(onIcon,  iconSize, QIcon::Selected, QIcon::On);
            icon.addFile(offIcon, iconSize, QIcon::Normal, QIcon::Off);
            icon.addFile(offIcon, iconSize, QIcon::Active, QIcon::Off);
            icon.addFile(offIcon, iconSize, QIcon::Disabled, QIcon::Off);
            icon.addFile(offIcon, iconSize, QIcon::Selected, QIcon::Off);
            font.setPointSize(8);
            pal.setColor(QPalette::ButtonText, QColor(0, 0, 0));
        } else {
            pal = btn->palette();
            const auto bgColor = tab.active
                ? tab.bgColor.lighter()
                : tab.bgColor.darker();
            pal.setColor(QPalette::Button, bgColor);
            pal.setColor(QPalette::ButtonText, tab.fgColor);
            font.setPointSize(24);
        }

        btn->setText(tab.label);
        btn->setIcon(icon);
        btn->setChecked(tab.active);

        const auto wt = tab.active
            ? QFont::Black
            : QFont::Light;

        font.setWeight(wt);
        btn->setFont(font);
        btn->setAutoFillBackground(true);
        btn->setPalette(pal);
        btn->update();
    }
}
