#pragma once
#include <QObject>
#include <QColor>

#include "types.h"

enum Screen {
    EMPTY,
    BANK,
    PRESET,
    UNIT
};

struct Tab {
    QColor bgColor;
    QColor fgColor;
    bool active;
    Screen type;
    QString name;
    QString label;
};

struct Page {
    Screen screen;
    QList<Tab> tabs;
};

constexpr quint8 NUM_BUTTONS = 3;

class State : public QObject
{
    Q_OBJECT
public:
    explicit State(QObject *parent = nullptr);


public slots:
    void ready();

    // Comes from JSONRPC
    void receiveBankList(const QStringList &banks);
    void receivePresetList(const QString &bank, const QStringList &presets);
    void receiveUnitList(const quint8 listNumber, const UnitList &units);
    void receiveCurrentBankPreset(const QString &bank, const QString &preset);
    void receiveUnitState(const QString &name, const quint8 state);

    // Comes from UI
    void onButtonPress(const quint8 idx);
    void onPrev();
    void onNext();
    void onPagePress();

signals:
    // Goes out to JSONRPC
    void unitSwitch(const QString &name, const bool state);
    void presetSelect(const QString &bank, const QString &name);
    void bankSelect(const QString &name);

    // Goes to UI
    void pageChange(const Page &page);

private:
    void calculateBankTabs();
    void calculatePresetTabs();
    void calculateUnitTabs();

    Page m_currentPage;

    QStringList m_bankList;
    quint8 m_bankPage;

    QStringList m_presetList;
    quint8 m_presetPage;
    QPair<QString, QString> m_currentBankPreset;

    UnitList m_monoUnitList;
    UnitList m_stereoUnitList;
    UnitList m_unitList;
    quint8 m_unitPage;
};
