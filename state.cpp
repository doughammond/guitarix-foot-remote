#include <QDebug>

#include "state.h"

Tab emptyTab() {
    return {
        QColor(0, 0, 0),
        QColor(0, 0, 0),
        false,
        Screen::EMPTY,
        "",
        ""
    };
}

State::State(QObject *parent) : QObject(parent)
{
    m_currentPage.screen = Screen::BANK;
    m_currentPage.tabs.insert(0, emptyTab());
    m_currentPage.tabs.insert(0, emptyTab());
    m_currentPage.tabs.insert(0, emptyTab());
}

void State::ready()
{
    emit pageChange(m_currentPage);
}

void State::receiveBankList(const QStringList &banks)
{
    m_bankList = banks;
    m_bankPage = 0;
    if (m_currentPage.screen == Screen::BANK) {
        calculateBankTabs();
        emit pageChange(m_currentPage);
    }
}

void State::receivePresetList(const QString &bank, const QStringList &presets)
{
    if (bank == m_currentBankPreset.first) {
        m_presetList = presets;
        m_presetPage = 0;
        if (m_currentPage.screen == Screen::PRESET) {
            calculatePresetTabs();
            emit pageChange(m_currentPage);
        }
    }
}

void State::receiveUnitList(const quint8 listNumber, const UnitList &units)
{
    auto &unitList = listNumber == 0 ? m_monoUnitList : m_stereoUnitList;
    unitList.clear();
    for (const auto &unit : units) {
        // cannot switch the Amp, so skip it as a unit
        if (unit.name != "ampstack") {
            unitList << unit;
        }
    }

    qDebug() << "state received a unit list" << listNumber << "units" << unitList.size();

    m_unitList.clear();
    for (const auto &unit : m_monoUnitList) {
        m_unitList.push_back(unit);
    }
    for (const auto &unit : m_stereoUnitList) {
        m_unitList.push_back(unit);
    }

    qDebug() << "state combined unit list is" << m_unitList.size();

    // qDebug() << "new unit list" << units;
    m_unitPage = 0;
    if (m_currentPage.screen == Screen::UNIT) {
        calculateUnitTabs();
        emit pageChange(m_currentPage);
    }
}

void State::receiveCurrentBankPreset(const QString &bank, const QString &preset)
{
    m_currentBankPreset = { bank, preset };
    if (m_currentPage.screen == Screen::BANK) {
        calculateBankTabs();
        emit pageChange(m_currentPage);
    }
    if (m_currentPage.screen == Screen::PRESET) {
        calculatePresetTabs();
        emit pageChange(m_currentPage);
    }
}

void State::receiveUnitState(const QString &name, const quint8 state)
{
    for (auto && u : m_unitList) {
        if (u.name == name) {
            qDebug() << "unit state matched with known unit" << name << " new state " << state;
            u.enabled = state;
        }
    }
    if (m_currentPage.screen == Screen::UNIT) {
        calculateUnitTabs();
        emit pageChange(m_currentPage);
    }
}

void State::onButtonPress(const quint8 idx)
{
    const auto tab = m_currentPage.tabs[idx];
    if (tab.name == "") {
        return;
    }

    switch (m_currentPage.screen) {
    case Screen::BANK: {
        emit bankSelect(tab.name);
        m_currentBankPreset = { tab.name, "" };
        m_currentPage.screen = Screen::PRESET;
        calculatePresetTabs();
        break;
    }
    case Screen::PRESET: {
        m_currentBankPreset.second = tab.name;
        emit presetSelect(m_currentBankPreset.first, m_currentBankPreset.second);
        m_currentPage.screen = Screen::UNIT;
        calculateUnitTabs();
        break;
    }
    case Screen::UNIT: {
        const auto lidx = idx + m_unitPage * NUM_BUTTONS;
        if (lidx < m_unitList.size()) {
            const auto nextState = 1 - m_unitList[lidx].enabled;
            emit unitSwitch(m_unitList[lidx].name, nextState);
        }
        break;
    }
    case Screen::EMPTY: {
        break;
    }
    }

    emit pageChange(m_currentPage);
}

quint8 nextPageNum(const quint8 currentPage, const int &numItems, const int direction) {
    const int next = currentPage + direction;

    qDebug() << "currentPage" << currentPage << " list size" << numItems << " direction" << direction << " next" << next;

    if (next < 0) {
        return 0;
    }
    if ((next * NUM_BUTTONS) >= numItems) {
        return currentPage;
    }
    return next;
}

void State::onPrev()
{
    switch (m_currentPage.screen) {
    case Screen::BANK:
        m_bankPage = nextPageNum(m_bankPage, m_bankList.size(), -1);
        calculateBankTabs();
        break;
    case Screen::PRESET:
        m_presetPage = nextPageNum(m_presetPage, m_presetList.size(), -1);
        calculatePresetTabs();
        break;
    case Screen::UNIT:
        m_unitPage = nextPageNum(m_unitPage, m_unitList.size(), -1);
        calculateUnitTabs();
        break;
    case Screen::EMPTY:
        break;
    }
    emit pageChange(m_currentPage);
}

void State::onNext()
{
    switch (m_currentPage.screen) {
    case Screen::BANK:
        m_bankPage = nextPageNum(m_bankPage, m_bankList.size(), 1);
        calculateBankTabs();
        break;
    case Screen::PRESET:
        m_presetPage = nextPageNum(m_presetPage, m_presetList.size(), 1);
        calculatePresetTabs();
        break;
    case Screen::UNIT:
        m_unitPage = nextPageNum(m_unitPage, m_unitList.size(), 1);
        calculateUnitTabs();
        break;
    case Screen::EMPTY:
        break;
    }
    emit pageChange(m_currentPage);
}

void State::onPagePress()
{
    switch (m_currentPage.screen) {
    case Screen::BANK:
        m_currentPage.screen = Screen::UNIT;
        calculateUnitTabs();
        break;
    case Screen::PRESET:
        m_currentPage.screen = Screen::BANK;
        calculateBankTabs();
        break;
    case Screen::UNIT:
        m_currentPage.screen = Screen::PRESET;
        calculatePresetTabs();
        break;
    case Screen::EMPTY:
        break;
    }
    emit pageChange(m_currentPage);
}

void State::calculateBankTabs()
{
    for (int i = 0; i < NUM_BUTTONS; ++i) {
        const auto idx = (m_bankPage * NUM_BUTTONS) + i;
        qDebug() << "calculate bank tab bankPage" << m_bankPage << " i" << i << " idx" << idx;
        if (idx < m_bankList.size()) {
            m_currentPage.tabs[i] = {
                QColor(255, 128, 64),
                QColor(0, 0, 0),
                m_bankList[idx] == m_currentBankPreset.first,
                Screen::BANK,
                m_bankList[idx],
                m_bankList[idx]
            };
        } else {
            m_currentPage.tabs[i] = emptyTab();
        }
    }
}

void State::calculatePresetTabs()
{
    for (int i = 0; i < NUM_BUTTONS; ++i) {
        const auto idx = (m_presetPage * NUM_BUTTONS) + i;
        if (idx < m_presetList.size()) {
            m_currentPage.tabs[i] = {
                QColor(64, 128, 255),
                QColor(0, 0, 0),
                m_presetList[idx] == m_currentBankPreset.second,
                Screen::PRESET,
                m_presetList[idx],
                m_presetList[idx]
            };
        } else {
            m_currentPage.tabs[i] = emptyTab();
        }
    }
}

void State::calculateUnitTabs()
{
    for (int i = 0; i < NUM_BUTTONS; ++i) {
        const auto idx = (m_unitPage * NUM_BUTTONS) + i;
        if (idx < m_unitList.size()) {
            m_currentPage.tabs[i] = {
                QColor(64, 255, 128),
                QColor(0, 0, 0),
                m_unitList[idx].enabled == 1,
                Screen::UNIT,
                m_unitList[idx].name,
                m_unitList[idx].label
            };
        } else {
            m_currentPage.tabs[i] = emptyTab();
        }
    }
}
